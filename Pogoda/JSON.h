//
//  JSON.h
//  Pogoda
//
//  Created by Przemek on 29.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Weather.h"

@interface JSON : NSObject

+ (Weather *)parseWeatherJSON:(NSData *)data error:(NSError *)errror;

@end
