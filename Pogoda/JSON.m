//
//  JSON.m
//  Pogoda
//
//  Created by Przemek on 29.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "JSON.h"

@implementation JSON

+ (Weather *)parseWeatherJSON:(NSData *)data error:(NSError *)error {
    NSError *localError = nil;
    NSDictionary *parsedData = [ NSJSONSerialization JSONObjectWithData:data options:0 error:&localError ];
    
    if (localError != nil) {
        error = localError;
        return nil;
    }
    
    Weather *weather = [ [Weather alloc] init ];
    int requestCode = [ (NSString *)[parsedData valueForKey:@"cod"] intValue ];
    
    if ( requestCode == 200 ) {
    
        NSMutableDictionary *weatherData = [ [NSMutableDictionary alloc] init ];
        NSArray *keys = @[@"coord", @"main", @"sys", @"wind"];
    
        for ( NSString *key in keys ) {
            [ weatherData addEntriesFromDictionary:[parsedData valueForKey:key]];
        }
    
        [ weatherData addEntriesFromDictionary:[(NSArray *)[parsedData valueForKey:@"weather"] objectAtIndex:0]];
        [ weatherData setValue:[parsedData valueForKey:@"name"] forKey:@"name" ];
    
        NSDictionary *weatherLoopData = @{
                @"city"         : @"name",
                @"country"      : @"country",
                @"temperature"  : @"temp",
                @"wind_deg"     : @"deg",
                @"wind_speed"   : @"speed",
                @"cloudiness"   : @"description",
                @"pressure"     : @"pressure",
                @"humidity"     : @"humidity",
                @"sunrise"      : @"sunrise",
                @"sunset"       : @"sunset",
                @"coords_lat"   : @"lat",
                @"coords_lon"   : @"lon",
                @"icon"         : @"icon",
                @"wid"          : @"id"
                                          };
        for ( NSString *key in weatherLoopData ) {
            [ weather setValue:[ weatherData valueForKey:[weatherLoopData valueForKey:key] ] forKey:key ];
        }
        
    } else {
        
        weather.error = [ parsedData valueForKey:@"message" ];
        
    }

    return weather;
}

@end
