//
//  ViewController.h
//  Pogoda
//
//  Created by Przemek on 28.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSON.h"

#define API_URL(X, Y) [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?q=%@&units=metric&lang=%@",(X), (Y)]

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *cityNameTextField;
@property (strong, nonatomic) IBOutlet UIImageView *wdIcon;
@property (strong, nonatomic) IBOutlet UILabel *wdCity;
@property (strong, nonatomic) IBOutlet UILabel *wdTemp;
@property (strong, nonatomic) IBOutlet UILabel *wdWind;
@property (strong, nonatomic) IBOutlet UILabel *wdCloudiness;
@property (strong, nonatomic) IBOutlet UILabel *wdPressure;
@property (strong, nonatomic) IBOutlet UILabel *wdHumidity;
@property (strong, nonatomic) IBOutlet UILabel *wdSunrise;
@property (strong, nonatomic) IBOutlet UILabel *wdSunset;
@property (strong, nonatomic) IBOutlet UILabel *wdCoords;

+ (NSString *) removeSuspiciousChars:(NSString *)string;
+ (NSString *)getTimeFromUnixFormat:(NSString *)unixTime;

@end

