//
//  ViewController.m
//  Pogoda
//
//  Created by Przemek on 28.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize cityNameTextField;

- (IBAction)checkWeather:(id)sender {
    
    NSURL *url = [ [NSURL alloc] initWithString:[ API_URL([ViewController removeSuspiciousChars:cityNameTextField.text], NSLocalizedString(@"api_lang", NULL)) stringByReplacingOccurrencesOfString:@" " withString:@"%20" ] ];
    
    [ NSURLConnection sendAsynchronousRequest:[ [NSURLRequest alloc] initWithURL:url ] queue:[ [ NSOperationQueue alloc] init ] completionHandler:^( NSURLResponse *response, NSData *data, NSError *error ) {
        if (error) {
            NSLog(@"%@", error.description );
        } else {
            NSError *error;
            Weather *weather = [JSON parseWeatherJSON:data error:error ];
            
            if (error != nil) {
                [ self displayAlert:NSLocalizedString(@"error_title", NULL) withMessage:error.localizedDescription buttonText:NSLocalizedString(@"error_ok", NULL) ];
            }
            
            if ( !weather.error ) {
                [ self displayWeatherData:weather ];
            } else {
                [ self displayAlert:NSLocalizedString(@"error_title", NULL) withMessage:weather.error buttonText:NSLocalizedString(@"error_ok", NULL) ];
            }
            
        }
    } ];
    
    [ self.view endEditing:YES ];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [ [UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent ];
    [ self registerForKbrdNotifications ];
    
    cityNameTextField.delegate = self;
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    [ [NSNotificationCenter defaultCenter] removeObserver:self ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [ self.view endEditing:YES ];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [ textField resignFirstResponder ];
    [ self checkWeather:NULL ];
    return NO;
}


- (void) registerForKbrdNotifications {
    
    [ [NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardUp:) name:UIKeyboardWillShowNotification object:nil ];
    [ [NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDown:) name:UIKeyboardWillHideNotification object:nil ];
    
}

- (void) keyboardUp:(NSNotification *)notification {
    
    NSDictionary *info      = [ notification userInfo ];
    CGSize        kbSize    = [ [info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [ self moveViewUpDown:kbSize.height down:NO ];
    
}

- (void) keyboardDown:(NSNotification *)notification {
    
    NSDictionary *info      = [ notification userInfo ];
    CGSize        kbSize    = [ [info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [ self moveViewUpDown:kbSize.height down:YES ];
    
}

- (void) moveViewUpDown:(int)offset down:(bool)down {
    
    [ UIView beginAnimations:nil context:NULL ];
    [ UIView setAnimationDuration:0.3 ];
    
    CGRect viewFrame = self.view.frame;
    
    if ( down ) {
        viewFrame.origin.y += offset;
    } else {
        viewFrame.origin.y = -offset;
    }
    
    [ self.view setFrame:viewFrame];
    [ UIView commitAnimations ];
    
}

+ (NSString *) removeSuspiciousChars:(NSString *)string {
    
    NSData *asciiEnc = [ string dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES ];
    
    string = [ [NSString alloc] initWithData:asciiEnc encoding:NSASCIIStringEncoding ];
    string = [ string stringByReplacingOccurrencesOfString:@"[\\\\/>\"'*();|:&^%$?#@!.,<]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [string length])];

    return string;
}

+ (NSString *)getTimeFromUnixFormat:(NSString *)unixTime {
    
    NSTimeInterval interval = [ unixTime doubleValue ];
    
    NSDate *unixDate = [ [NSDate alloc] initWithTimeIntervalSince1970:interval ];
    NSDateFormatter *dateFormatter = [ [ NSDateFormatter alloc] init ];
    
    [dateFormatter setDateFormat:@"HH:mm" ];
    
    return [dateFormatter stringFromDate:unixDate ];
}

- (void) displayAlert:(NSString *)title withMessage:(NSString *)message buttonText:(NSString *)bttext {
    
    UIAlertController *alert = [ UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert ];
    
    [ alert addAction:[ UIAlertAction actionWithTitle:bttext style:UIAlertActionStyleDefault handler:^( UIAlertAction *action ) {
        [ self dismissViewControllerAnimated:YES completion:nil ];
    }]];
    
    [ self presentViewController:alert animated:YES completion:nil ];
    
}

- (void) displayWeatherData:(Weather *)weather {
    NSLog(@"%@", weather.icon);
    dispatch_async(dispatch_get_main_queue(), ^{
        _wdCity.text       = [ NSString stringWithFormat:@"%@, %@", weather.city, weather.country ];
        _wdTemp.text       = [ NSString stringWithFormat:@"%@%ld", ( (lroundf([weather.temperature floatValue]) < 10) && (lroundf([weather.temperature floatValue]) >=0 ) ) ? @"0" : @"", lroundf([weather.temperature floatValue])];
        _wdWind.text       = [ NSString stringWithFormat:@"%lu %@@%.2f m/s", lroundf( [weather.wind_deg floatValue] ), NSLocalizedString(@"degree", NULL),[ weather.wind_speed floatValue ] ];
        _wdCloudiness.text = [ NSString stringWithFormat:@"%@", weather.cloudiness ];   // Ze względu na sporadycznie występujące wyjątki przy bezpośrednim odwołaniu.
        _wdPressure.text   = [ NSString stringWithFormat:@"%@ hPa", weather.pressure ];
        _wdHumidity.text   = [ NSString stringWithFormat:@"%@ %%", weather.humidity ];
        _wdSunrise.text    = [ ViewController getTimeFromUnixFormat:weather.sunrise ];
        _wdSunset.text     = [ ViewController getTimeFromUnixFormat:weather.sunset ];
        _wdCoords.text     = [ NSString stringWithFormat:@"%.2f, %.2f", [weather.coords_lat floatValue], [weather.coords_lon floatValue] ];
        
        _wdIcon.image = [ UIImage imageNamed:weather.icon ];
    });
    
}



@end
