//
//  Weather.h
//  Pogoda
//
//  Created by Przemek on 29.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weather : NSObject

@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *temperature;
@property (strong, nonatomic) NSString *wind_deg;
@property (strong, nonatomic) NSString *wind_speed;
@property (strong, nonatomic) NSString *cloudiness;
@property (strong, nonatomic) NSString *pressure;
@property (strong, nonatomic) NSString *humidity;
@property (strong, nonatomic) NSString *sunrise;
@property (strong, nonatomic) NSString *sunset;
@property (strong, nonatomic) NSString *coords_lat;
@property (strong, nonatomic) NSString *coords_lon;
@property (strong, nonatomic) NSString *icon;
@property (strong, nonatomic) NSString *wid;
@property (strong, nonatomic) NSString *error;

@end
